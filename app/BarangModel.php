<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BarangModel extends Model
{
    protected $table = 'barang';

    protected $fillable =[
        'nama_barang','harga_satuan','stok','keterangan',
    ];

    public function haveSuplier(){
        return $this->beLongsTo(SuplierModel::class, 'id_suplier', 'id_suplier');
    }
}
