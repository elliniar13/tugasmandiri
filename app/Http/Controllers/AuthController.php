<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Session;
use Auth;
use Hash;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    public function __construct(){
        $this->middleware('guest')->except('logout');
    }

    public function index(){
        return view('auth.login');
    }

    public function sendLoginRequest(Request $request, User $user){
        $email = $request->email;
        $password = $request->password;

        $data = $user::where('email', $email)->first();
        if($data){
        if(Hash::check($password, $data->password)){
            Session::put('name',$data->name);
            Session::put('email',$data->email);
            Session::put('login',TRUE);

            return redirect()-> route('tampil_barang');
        }else{
            return redirect()-> back()->with('error', 'Invalid email or password');
        }
    }else{
        return redirect()-> back()->with('error','No data found');
    }
}
 
public function logout(Request $request)
{
    if(\Auth::check())
    {
        \Auth::logout();
        $request->session()->invalidate();
    }
return redirect()->route('login_form');
}
}

