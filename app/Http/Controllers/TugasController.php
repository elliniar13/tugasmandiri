<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TugasController extends Controller
{
    public function index(){
        return view('formtugas');
    }

    public function nilai(Request $request){

        // dd($request);
        $a = $request -> angka;
        if($a>80){
        return redirect('/tugas')->with('notip','Nilai anda : '.$a. ' Grade anda A');
        }
        elseif($a>=70&&$a<80){
        return redirect('/tugas')->with('notip','Nilai anda : '.$a. ' Grade anda B');
        }
        elseif($a>=60&&$a<70){
        return redirect('/tugas')->with('notip','Nilai anda : '.$a. ' Grade anda C');
        }
        elseif($a<60){
        return redirect('/tugas')->with('notip','Nilai anda : '.$a. ' Grade anda D');
        }
    }
}
