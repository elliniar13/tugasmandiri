<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KalkulatorController extends Controller
{

    //return text
    // public function index(){
    //     return'Halo laravel from controller';
    // }

    //return view
    // public function index(){
    //     return view('welcome');
    // }

    public function index(){
        return view('forminput');
    }

    // public function nilai(Request $request){
    //     $a = $request-> angka;
    //     if($a == "A"){
    //         echo "Sangat Baik";
    //     }  elseif($a == "B"){
    //         echo "Baik";
    //     }elseif($a == "C"){
    //         echo "Cukup Baik";
    //     }elseif($a == "D"){
    //         echo "Buruk Banget ih";
    //     }


        // dd($request);
        // $a = $request -> angka;
        // if($a>=80){
        // return redirect('/kalkulator')->with('notip','Nilai anda : '.$a. ' Grade anda A');
        // }
        // elseif($a>=70&&$a<80){
        // return redirect('/kalkulator')->with('notip','Nilai anda : '.$a. ' Grade anda B');
        // }
        // elseif($a>=60&&$a<70){
        // return redirect('/kalkulator')->with('notip','Nilai anda : '.$a. ' Grade anda C');
        // }
        // elseif($a<60){
        // return redirect('/kalkulator')->with('notip','Nilai anda : '.$a. ' Grade anda D');
        // }
    

    public function tambah(Request $request){

        // dd($request);
        $a = $request -> variabelsatu;
        $b = $request -> variabeldua;
 
        $c = $a + $b;
        return $c;

    }


    public function bagi(Request $request){

        // dd($request);
        $a = $request -> variabelsatu;
        $b = $request -> variabeldua;
 
        $c = $a / $b;
        return $c;

    }

    public function kurang(Request $request){

        // dd($request);
        $a = $request -> variabelsatu;
        $b = $request -> variabeldua;
 
        $c = $a - $b;
        return $c;

    }
}