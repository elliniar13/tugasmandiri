<?php

use Illuminate\Database\Seeder;

class usersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users') -> insert(array(
            array(
                'name' => 'Elin',
                'email' => 'Ellin@gmail.com',
                'password' => bcrypt('123456'),
            ),
            array(
                'name' => 'siapa',
                'email' => 'siapa@gmail.com',
                'password' => bcrypt('123456'),
            ),
        ));
    }
}
