<?php

use Illuminate\Database\Seeder;

class BukuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('buku') -> insert(array(
            array(
                'nama_buku' => 'Handuk',
                'harga_barang' => 30000,
                'stok' => 10,
                'keterangan' => 'Produk Terbaik',
                'id_suplier' => 'sup-001',
                'is_active' => 1,
                'created_at' => now(),
            ),
            array(
                'nama_barang' => 'Sabun Mandi',
                'harga_barang' => 30000,
                'stok' => 15,
                'keterangan' => 'Produk Terlaris',
                'id_suplier' => 'sup-002',
                'is_active' => 1,
                'created_at' => now(),
            ),
        ));
    }
}
