<?php

use app\mahasiswa;

//return text
Route::get('/', function () {
    return 'Hello Laravel';
});


//return view
Route::get('/fromview', function () {
    return view('hello');
});

//return model
Route::get('/frommodel', function () {
    $data = mahasiswa::all();
    return $data;
});

// Route::get('/tugas','TugasController@index');
// Route::post('/hasil', 'TugasController@nilai')-> name('proses.nilai');

Route::get ('/login','AuthController@index')->name('login_form');
Route::post('/sendlogin','AuthController@sendLoginRequest')->name('login_action');
Route::get('/logout','AuthController@logout')->name('logout_action');

// Route::group(['middleware' => 'auth'], function () {
Route::get('/kalkulator','KalkulatorController@index');
Route::post('/proses', 'KalkulatorController@tambah')-> name('proses.tambah');
Route::post('/bagi', 'KalkulatorController@bagi')-> name('proses_bagi');
Route::post('/kurang', 'KalkulatorController@kurang')-> name('proses-kurang');

Route::get('/barang','BarangController@index')->name('tampil_barang');
Route::get('barang/create','BarangController@createData')-> name('create_barang');
Route::post('barang/post','BarangController@postData')->name('post_barang');

Route::get('barang/edit/{id}','BarangController@editData')->name('edit_data');
Route::post('barang/update/{id}','BarangController@updateData')->name('update_data');
Route::get('barang/Hapus/{id}','BarangController@HapusData')->name('HapusData');
// });