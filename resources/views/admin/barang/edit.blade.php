@extends ('admin.layouts.master')
@section('content')
<div class="col-12 col-md-6 col-lg-6">
                <div class="card">
                  <div class="card-header">
                    <h4>Edit Data</h4>
                  </div>
                <form action="{{route ('update_data, $data->kode_barang') }}">
                  <div class="card-body">
                    <div class="form-group">
                      <label>Nama Barang</label>
                      <input type="text" class="form-control" name="nama_barang" value="{{$data->nama_barang}}">
                    </div>
                    <div class="form-group">
                      <label>Harga Satuan</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <div class="input-group-text">
                            $
                          </div>
                        </div>
                        <input type="text" class="form-control currency" nama="harga_satuan" id="harga_satuan" value="{{$data->harga-satuan}}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Stok</label>
                      <input type="text" class="form-control" name="stok_barang" id="stok_barang" value="{{$data->stok}}">
                    </div>
                    <div class="form-group">
                      <label>Keterangan</label>
                      <input type="text" class="form-control" name="keterangan" id="keterangan" value="{{$data->keterangan}}">
                    </div>
                </form>
                <div class="card">
                  <div class="card-header">
                    <h4>Toggle Switches</h4>
                  </div>
                  <div class="card-body">
                    <div class="form-group">
                      <div class="control-label">Toggle switches</div>
                      <div class="custom-switches-stacked mt-2">
                        <label class="custom-switch">
                          <input type="radio" name="option" value="1" class="custom-switch-input" checked="">
                          <span class="custom-switch-indicator"></span>
                          <span class="custom-switch-description">Option 1</span>
                        </label>
                        <label class="custom-switch">
                          <input type="radio" name="option" value="2" class="custom-switch-input">
                          <span class="custom-switch-indicator"></span>
                          <span class="custom-switch-description">Option 2</span>
                        </label>
                        <label class="custom-switch">
                          <input type="radio" name="option" value="3" class="custom-switch-input">
                          <span class="custom-switch-indicator"></span>
                          <span class="custom-switch-description">Option 3</span>
                        </label>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="control-label">Toggle switch single</div>
                      <label class="custom-switch mt-2">
                        <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input">
                        <span class="custom-switch-indicator"></span>
                        <span class="custom-switch-description">I agree with terms and conditions</span>
                      </label>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header">
                    <h4>Image Check</h4>
                  </div>
                  <div class="card-body">
                    <div class="form-group">
                      <label class="form-label">Image Check</label>
                      <div class="row gutters-sm">
                        <div class="col-6 col-sm-4">
                          <label class="imagecheck mb-4">
                            <input name="imagecheck" type="checkbox" value="1" class="imagecheck-input">
                            <figure class="imagecheck-figure">
                              <img src="../assets/img/news/img01.jpg" alt="}" class="imagecheck-image">
                            </figure>
                          </label>
                        </div>
                        <div class="col-6 col-sm-4">
                          <label class="imagecheck mb-4">
                            <input name="imagecheck" type="checkbox" value="2" class="imagecheck-input" checked="">
                            <figure class="imagecheck-figure">
                              <img src="../assets/img/news/img02.jpg" alt="}" class="imagecheck-image">
                            </figure>
                          </label>
                        </div>
                        <div class="col-6 col-sm-4">
                          <label class="imagecheck mb-4">
                            <input name="imagecheck" type="checkbox" value="3" class="imagecheck-input">
                            <figure class="imagecheck-figure">
                              <img src="../assets/img/news/img03.jpg" alt="}" class="imagecheck-image">
                            </figure>
                          </label>
                        </div>
                        <div class="col-6 col-sm-4">
                          <label class="imagecheck mb-4">
                            <input name="imagecheck" type="checkbox" value="4" class="imagecheck-input" checked="">
                            <figure class="imagecheck-figure">
                              <img src="../assets/img/news/img04.jpg" alt="}" class="imagecheck-image">
                            </figure>
                          </label>
                        </div>
                        <div class="col-6 col-sm-4">
                          <label class="imagecheck mb-4">
                            <input name="imagecheck" type="checkbox" value="5" class="imagecheck-input">
                            <figure class="imagecheck-figure">
                              <img src="../assets/img/news/img05.jpg" alt="}" class="imagecheck-image">
                            </figure>
                          </label>
                        </div>
                        <div class="col-6 col-sm-4">
                          <label class="imagecheck mb-4">
                            <input name="imagecheck" type="checkbox" value="6" class="imagecheck-input">
                            <figure class="imagecheck-figure">
                              <img src="../assets/img/news/img06.jpg" alt="}" class="imagecheck-image">
                            </figure>
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header">
                    <h4>Color</h4>
                  </div>
                  <div class="card-body">
                    <div class="form-group">
                      <label>Simple</label>
                      <input type="text" class="form-control colorpickerinput">
                    </div>
                    <div class="form-group">
                      <label>Pick Your Color</label>
                      <div class="input-group colorpickerinput">
                        <input type="text" class="form-control">
                        <div class="input-group-append">
                          <div class="input-group-text">
                            <i class="fas fa-fill-drip"></i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="form-label">Color Input</label>
                      <div class="row gutters-xs">
                        <div class="col-auto">
                          <label class="colorinput">
                            <input name="color" type="checkbox" value="primary" class="colorinput-input">
                            <span class="colorinput-color bg-primary"></span>
                          </label>
                        </div>
                        <div class="col-auto">
                          <label class="colorinput">
                            <input name="color" type="checkbox" value="secondary" class="colorinput-input">
                            <span class="colorinput-color bg-secondary"></span>
                          </label>
                        </div>
                        <div class="col-auto">
                          <label class="colorinput">
                            <input name="color" type="checkbox" value="danger" class="colorinput-input">
                            <span class="colorinput-color bg-danger"></span>
                          </label>
                        </div>
                        <div class="col-auto">
                          <label class="colorinput">
                            <input name="color" type="checkbox" value="warning" class="colorinput-input">
                            <span class="colorinput-color bg-warning"></span>
                          </label>
                        </div>
                        <div class="col-auto">
                          <label class="colorinput">
                            <input name="color" type="checkbox" value="info" class="colorinput-input">
                            <span class="colorinput-color bg-info"></span>
                          </label>
                        </div>
                        <div class="col-auto">
                          <label class="colorinput">
                            <input name="color" type="checkbox" value="success" class="colorinput-input">
                            <span class="colorinput-color bg-success"></span>
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
@endsection