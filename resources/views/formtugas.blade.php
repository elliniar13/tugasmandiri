<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TUGAS</title>
</head>

<body>
    <h2>Penilaian</h2>

    <form action="{{ route('proses.nilai') }}" method="post">
        @csrf
        <input type="text" name="angka" id="angka"><br><br>

        <input type="submit" value="Nilai">
    </form>
    @if(session('notip'))
    <h3>{{session('notip')}}</h3>
    @endif
</body>

</html>